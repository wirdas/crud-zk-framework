package com.crud.viewmodel;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.Textbox;

import com.crud.dao.MdepartementDao;
import com.crud.domain.Mdepartement;

public class MdepartmentVm {

	public Mdepartement dept;
	public MdepartementDao objDao = new MdepartementDao();

	@Wire
	Grid grid;
	
	@Wire
	Textbox txtSearch, txtdeptCode, txtdeptName;

	@AfterCompose
	private void afterCompose(@ContextParam(ContextType.VIEW) Component component) {

		Selectors.wireComponents(component, this, false);
		dept = new Mdepartement();
		doRefresh();

		grid.setRowRenderer(new RowRenderer<Mdepartement>() {
			@Override
			public void render(Row row, Mdepartement data, int i) throws Exception {
				// TODO Auto-generated method stub
				
				row.appendChild(new Label(String.valueOf(data.getmDepartementPk())));
				row.appendChild(new Label(data.getDeptCode()));
				row.appendChild(new Label(data.getDeptName()));
				
				Button rmv = new Button("DELETE");
				Button edt = new Button("EDIT");
				
				rmv.addEventListener(Events.ON_CLICK, new EventListener<>() {
					@Override
					public void onEvent(Event arg0) throws Exception {
						Messagebox.show("Are you sure to delete this list ?", "Confirm Delete", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener<>() {
							@Override
							public void onEvent(Event arg0) throws Exception {
								dept = data;
								objDao.delete(dept);
								doRefresh();
							}
						});
					}
				});
				
				edt.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

					@Override
					public void onEvent(Event arg0) throws Exception {
						
						dept = data;
						BindUtils.postNotifyChange(null, null, MdepartmentVm.this, "dept");
						
					}
				});
				
				Hlayout hlayout = new Hlayout();
				hlayout.appendChild(edt);
				hlayout.appendChild(rmv);
				row.appendChild(hlayout);
			}
		});
	}
	
	@Command
	public void doSave() {
		objDao.save(dept);
		doRefresh();
	}
	
	@Command
	public void doCancel() {
		doRefresh();
	}
	
	@Command
	public void doSearch() {
		dept = new Mdepartement();
		grid.setModel(new ListModelList<>(objDao.list(txtSearch.getValue())));		
	}

	public void doRefresh() {
		grid.setModel(new ListModelList<>(objDao.list("")));
		txtSearch.setValue(null);
	}

	public Mdepartement getDept() {
		return dept;
	}

	public void setDept(Mdepartement dept) {
		this.dept = dept;
	}
}