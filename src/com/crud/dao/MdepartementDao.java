package com.crud.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.crud.domain.Mdepartement;

public class MdepartementDao {
	SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
	Session session;
	
	public List<Mdepartement> list(String valSearch) {
		valSearch = (valSearch == "") ? "from Mdepartement" : "from Mdepartement WHERE deptCode = '"+ valSearch +"' OR deptName = '"+ valSearch +"'";		
		List<Mdepartement> objList = new ArrayList<>();
		session = sessionFactory.openSession();
		objList = session.createQuery(valSearch).list();
		session.close();
		return objList;
	}
	
	public void save(Mdepartement data) {
		session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		session.saveOrUpdate(data);
		trx.commit();
		session.close();
	}
	
	public void delete(Mdepartement data) {
		session = sessionFactory.openSession();
		Transaction trx = session.beginTransaction();
		session.delete(data);
		trx.commit();
		session.close();
	}
}
