package com.crud.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Mdepartement {
	
	public Integer mDepartementPk;
	public String deptCode;
	public String deptName;
	
	@Id
	@SequenceGenerator(name = "MDEPARTMENT_MDEPARTMENTPK_GEN", sequenceName = "MDEPARTMENT_SEQ", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MDEPARTMENT_MDEPARTMENTPK_GEN")
	@Column(unique = true, nullable = false)
	public Integer getmDepartementPk() {
		return mDepartementPk;
	}
	public void setmDepartementPk(Integer mDepartementPk) {
		this.mDepartementPk = mDepartementPk;
	}
	public String getDeptCode() {
		return deptCode;
	}
	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
}